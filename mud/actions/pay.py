from .action import Action3
from mud.events import PayWithEvent

class PayWithAction(Action3):
	EVENT = PayWithEvent
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"
	ACTION = "pay-with"