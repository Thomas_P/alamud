# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import SpeakEvent

class SpeakAction(Action3):
    EVENT = SpeakEvent
    ACTION = "speak"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object):
        super().__init__(subject, "personne", object)

    def resolve_object2(self):
        return self.object2
