# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class SpeakEvent(Event3):
    NAME = "speak"

    def perform(self):
        self.add_prop("spoke-"+self.object2)
        self.inform("speak")
